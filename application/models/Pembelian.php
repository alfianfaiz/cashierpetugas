<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Model {

	public function getAllUnitBoughtByUserID($user_id){
		$sql = "SELECT 
			*, pembelian.create_date as tgl_transaksi
			FROM 
			`unit_barang` 
			LEFT JOIN 
				`pembelian` 
				ON `unit_barang`.`pembelian_id_pembelian` = `pembelian`.`id_pembelian`
			LEFT JOIN 
				barang
				ON unit_barang.barang_id_barang = barang.id_barang
			LEFT JOIN 
				user
				ON pembelian.id_petugas = user.id_user
			WHERE 
				pembelian.id_member = $user_id
				";
		return $this->db->query($sql)->result();
	}

}

/* End of file Pembelian.php */
/* Location: ./application/models/Pembelian.php */