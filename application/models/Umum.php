<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Umum extends CI_Model {

	public function getList($kriteria,$cek)
	{
		$data = $this->getListKelasKriteria($kriteria);
			echo "<option>Pilih Kriteria</option>";
		foreach ($data as $key)
		{
			$selected= "";
			if($key->id == $cek)
			{
				$selected = "selected=selected";
			}
			echo "<option $selected value=\"$key->id\">$key->nama_kelas</option>";
		}
	}
	

	public function getListPuskesmas($cek)
	{
		$data_puskesmas = $this->db->get('puskesmas')->result();
			echo "<option>Pilih Kelurahan</option>";
		foreach ($data_puskesmas as $key)
		{
			$selected= "";
			if($key->id == $cek)
			{
				$selected = "selected=selected";
			}
			echo "<option $selected value=\"$key->id\">$key->kelurahan</option>";
		}
	}

	private function getListKelasKriteria($kriteria)
	{
		return $this->db->select('kelas_kriteria.nama_kelas,kelas_kriteria.id')
								->where('kriteria',$kriteria)
								->join('kelas_kriteria','kelas_kriteria.kriteria_id = kriteria.id','LEFT')
								->get('kriteria')->result();
	}

}

/* End of file Umum.php */
/* Location: ./application/models/Umum.php */