<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Model {

	public function getall($keyword = null)
	{
		// $this->db->where('', '2');
		$this->db->join('barang', 'barang.id_barang = unit_barang.barang_id_barang', 'left');
		$this->db->join('jenis', 'barang.jenis_id_jenis = jenis.id_jenis', 'left');
		$this->db->join('warna', 'barang.warna_id_warna = warna.id_warna', 'left');
		$this->db->join('bahan', 'barang.bahan_id_bahan = bahan.id_bahan', 'left');
		$this->db->join('pembelian', 'unit_barang.pembelian_id_pembelian = pembelian.id_pembelian', 'left');
		$this->db->join('user', 'pembelian.id_member = user.id_user', 'left');
		if ($keyword != null) {

			$this->db->like('jenis.nama_jenis',$keyword);
		}
		$data = $this->db->get('unit_barang')->result();
		return $data;
	}

	public function getDataBarang($id_unit_barang){
		$this->db->where('id_unit_barang', $id_unit_barang);
		$this->db->join('barang', 'barang.id_barang = unit_barang.barang_id_barang', 'left');
		$this->db->join('jenis', 'barang.jenis_id_jenis = jenis.id_jenis', 'left');
		$this->db->join('warna', 'barang.warna_id_warna = warna.id_warna', 'left');
		$this->db->join('bahan', 'barang.bahan_id_bahan = bahan.id_bahan', 'left');
		return $this->db->get('unit_barang')->result();
	}

	public function getDataBarangByNomorUnit($nomor_unit){
		$this->db->where('nomor_unit', $nomor_unit);
		$this->db->where('pembelian_id_pembelian', NULL);
		$this->db->join('barang', 'barang.id_barang = unit_barang.barang_id_barang', 'left');
		$this->db->join('jenis', 'barang.jenis_id_jenis = jenis.id_jenis', 'left');
		$this->db->join('warna', 'barang.warna_id_warna = warna.id_warna', 'left');
		$this->db->join('bahan', 'barang.bahan_id_bahan = bahan.id_bahan', 'left');
		return $this->db->get('unit_barang')->result();
	}

}

/* End of file Barang.php */
/* Location: ./application/models/Barang.php */