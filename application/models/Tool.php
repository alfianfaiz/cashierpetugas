<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tool extends CI_Model {

	public function	getJenisList()
	{
		return $this->db->get('jenis')->result();
	}

	public function	getWarnaList()
	{
		return $this->db->get('warna')->result();
	}

	public function	getBahanList()
	{
		return $this->db->get('bahan')->result();
	}

}

/* End of file Tool.php */
/* Location: ./application/models/Tool.php */