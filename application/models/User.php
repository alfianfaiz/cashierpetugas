<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {

	public function getall()
	{
		$this->db->where('level', '2');
		$this->db->where('status', 1);
		$data = $this->db->get('user')->result();
		return $data;
	}	

	public function getDataUser($id_user)
	{
		$this->db->where('id_user', $id_user);
		return $this->db->get('User')->result();
	}

	public function getDataUserByRFID($rfid)
	{
		$this->db->where('kode_pengguna', $rfid);
		$this->db->where('level', 3);
		return $this->db->get('User')->result();
	}

	public function getUserIDByRFID($rfid)
	{
		$this->db->where('kode_pengguna', $rfid);
		$this->db->where('level', 3);
		$datauser = $this->db->get('User',1)->result();
		foreach ($datauser as $key) {
			return $key->id_user;
		}
		return 0;
	}



}

/* End of file User.php */
/* Location: ./application/models/User.php */