<?php 
	$asset = base_url().'assets/';
 ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $asset ?>css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $asset ?>css/costom.css">

	<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">	
	<script type="text/javascript" src="<?php echo $asset ?>js/jquery.js"></script>
	<title>Cashier</title>
</head>
<body>

<div class="container">
<!-- header -->
	<div  class="header">
			<center><font style="font-family:Lato;font-size:5em;"> Cashier</font><small><b>Outlet</b>Version</small></center>

	</div>
	<hr><br>
	<!-- <div class="navbar normalbox noborderradius">
		<div class="container-fluid ">
			<ul class="nav navbar-nav">
				<li><a href="<?php echo base_url() ?>home/"> <span class="glyphicon glyphicon-home"></span> Beranda</a></li>
				<li><a href="<?php echo base_url() ?>home/panduan"><span class="glyphicon glyphicon-th-large"></span> Panduan</a></li>
				<li><a href="<?php echo base_url() ?>home/kontak"><span class="glyphicon glyphicon-envelope"></span> Kontak Kami</a></li>
				<?php if (!$this->session->userdata('IS_LOGGED_IN')): ?>
					<li><a href="<?php echo base_url() ?>home/pendaftaran"><span class="glyphicon glyphicon-edit"></span> Pendaftaran</a></li>
					<li><a href="<?php echo base_url() ?>auth/out">Logout</a></li>
				<?php endif ?>
				
			</ul>
		</div>
	</div> -->
	
	<div class="row">
		<div class="col-md-3 " id="sidebar" >
		<div class="container-fluid normalbox leftnavbox" style="min-height:auto !important;padding:10px 10px;">
			<div class="media">
			  <div class="media-left ">
			    <a href="#">
			      <img class="media-object" src="<?php echo $asset ?>image/20160622093015.jpg" width="60px">
			    </a>
			  </div>
			  <div class="media-body">
			    <h4 class="media-heading">Nama Petugas</h4>
			    <small>Lorem ipsum dolor sit amet, consectetur adipi</small>
			  </div>
			</div>
		</div>
		<br>
			<ul class="nav nav-pills nav-stacked normalbox leftnavbox" data-spy="affix" data-offset-top="265">
					<li><a href="<?php echo base_url() ?>administration/penjualan"> <span class="glyphicon glyphicon-pencil"></span> Penjualan Hari ini</a></li>
					<li><a href="<?php echo base_url() ?>administration/transaksi"> <span class="glyphicon glyphicon-unchecked"></span>Transaksi</a></li>
					<li><a href="<?php echo base_url() ?>administration/membership"> <span class="glyphicon glyphicon-user"></span>Membership</a></li>
					<li><a href="<?php echo base_url() ?>auth/out"> <span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>	
		</div>
		<div class="col-md-9 normalbox normalpadding" style="min-height:500px;margin-bottom:100px">
			<?php $this->load->view('page/'.$page) ?>
		</div>
	</div>
	<div class="footerku">
		
	</div>
</div>

<script type="text/javascript" src="<?php echo $asset ?>js/bootstrap.js"></script>
</body>
</html>

