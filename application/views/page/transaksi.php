<div class="fluid-container">
	<h3>Transaksi</h3>
	<hr>
	<h4>Beli Barang</h4>
	<dd>Masukkan Kode RFID Barang</dd>
	<form class="fluid-container" method="post" action="<?php echo base_url() ?>/administration/addBarang/">
		<div class="row">
			<div class="col-md-3">
				<input type="text" name="rfid_barang" placeholder="x-x-x-x-x" class="form-control">
			</div>
			<div class="col-md-3 ">
				<button class="btn btn-default">Tambah ke Keranjang</button>
			</div>
			<div class="col-md-12">
			<span class="alert-danger">
				<?php echo $this->session->flashdata('error'); ?>
			</span>
				<hr>
			</div>
		</div>
	</form>
</div>
<div class="fluid-container">
	<h4>Keranjang Belanja</h4>
	<table class="table">
		<thead>
			<tr>
				<th>ID Barang</th>
				<th>Nama Barang</th>
				<th>Jenis</th>
				<th>Warna</th>
				<th>Ukuran</th>
				<th>Harga</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<?php $price = 0; ?>
			<?php foreach ($this->cart->contents() as $key): ?>
				<tr>
					<td>ID Barang</td>
					<td><?php echo $key['name'] ?></td>
					<td><?php echo $key['options']['Jenis'] ?></td>
					<td><?php echo $key['options']['Warna'] ?></td>
					<td><?php //echo $key['options']['ukuran'] ?></td>
					<td>Rp <?php $price = $price + $key['price']; echo $this->cart->format_number($key['price']) ?></td>
					<td><a href="<?php echo base_url() ?>administration/removebarang/<?php echo $key['rowid'] ?>" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-minus"></span></a></td>
				</tr>
			<?php endforeach ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="5">
					<h5>Total Harga</h5>
				</td>
				<td><h5><b>Rp. <?php echo  $this->cart->format_number($price) ?></b></h5></td>
			</tr>
			<tr>
				<td colspan="5">
					<!-- <h5>Total Harga</h5> -->
				</td>
				<td><a href="<?php echo base_url() ?>administration/checkout" class="btn btn-warning">Checkout</a></td>
			</tr>
		</tfoot>
	</table>

</div>