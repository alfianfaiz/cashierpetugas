<?php 
	
	
 ?>
<div class="fluid-container">
	<center><h2>Data Member</h2></center>
	<hr>
	<center>
		<form class="form-inline" method="get">
			<div class="form-group">
			    <input type="text" class="form-control" placeholder="Kode Member" name="rfid">
			</div>
			<div class="form-group">
			    <button class="btn btn-default">Cari Member</button>
			</div>
		</form><br>
	</center>
	<div class="normalbox" style="padding:10px;">
		<h4>Detail</h4>
		<span class="alert-success">
			<?php echo $this->session->flashdata('success'); ?>
		</span>
		<span class="alert-danger">
			<?php echo $this->session->flashdata('error'); ?>
		</span>
		<?php foreach ($datamember as $key): ?>
			<form method="post" action="<?php echo base_url() ?>administration/updatemember">
			<input type="hidden" name="id_member" value="<?php echo $key->id_user ?>">
			<input type="hidden" name="changeusername" id="changeusername" value="0">
				<table class="table">
					<tr>
						<td>Nama</td>
						<td>:</td>
						<td><input type="text" name="nama_lengkap" id="nama_lengkap" value="<?php echo $key->nama_lengkap ?>" class="form-control" readonly></td>
					</tr>
					<tr>
						<td>Kode Member</td>
						<td>:</td>
						<td><input type="text" name="kode_pengguna" id="kode_pengguna" value="<?php echo $key->kode_pengguna ?>" class="form-control" readonly></td>
					</tr>
					<tr>
						<td>Nomor Identitas</td>
						<td>:</td>
						<td><input type="text" name="no_identitas" id="no_identitas" value="<?php echo $key->no_identitas ?>" class="form-control" readonly></td>
					</tr>
					<tr>
						<td>Email</td>
						<td>:</td>
						<td><input type="text" name="email" id="email" value="<?php echo $key->email ?>" class="form-control" readonly></td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>:</td>
						<td>
							<textarea type="text" name="alamat" id="alamat" class="form-control" readonly><?php echo $key->alamat ?></textarea>
						</td>
					</tr>
					<tr>
						<td>Foto</td>
						<td>:</td>
						<td><?php  ?></td>
					</tr>
					<tr>
						<td colspan="2"><b>Username & Password</b></td>
						<td> <button class="btn btn-success" type="button" disabled id="btnchange">Change Username & Password</button></td>
					</tr>
					<tr>
						<td>Username</td>
						<td>:</td>
						<td><input type="text" name="username" id="username" value="<?php echo $key->username ?>" class="form-control" readonly></td>
					</tr>
					<tr>
						<td>Password</td>
						<td>:</td>
						<td><input type="password" name="password" id="password" class="form-control" readonly placeholder="fill this field to change password"></td>
					</tr>
					<tr> 
						<td colspan="3"> 
							<div class="btn-group pull-right"> 
								<button class="btn btn-success" type="submit" id="btnsimpan" disabled>Simpan Data</button> 
								<button class="btn btn-warning" type="button" id="btnedit">Edit Data ! </button> 
							</div>
						</td>
					</tr>
				</table>
			</form>

			<?php 
				$datapembelian = $this->Pembelian->getAllUnitBoughtByUserID($key->id_user);
			 ?>

			<table class="table">
				<tr>
					<td>Nama Barang</td>
					<td>Kode Barang</td>
					<td>Tanggal Pembelian</td>
					<td>Petugas</td>
				</tr>
				<?php foreach ($datapembelian as $key2): ?>
					<tr>
						<td><?php echo $key2->nama_barang ?></td>
						<td><?php echo $key2->nomor_unit ?></td>
						<td><?php echo $key2->tgl_transaksi ?></td>
						<td><?php echo $key2->nama_lengkap ?></td>
					</tr>
				<?php endforeach ?>
			</table>
		<?php endforeach ?>
		<!-- <table class="table">
			<tr>
				<td>Nama</td>
				<td>:</td>
				<td><input type="text" class="form-control" name="nama_member"></td>
			</tr>
			<tr>
				<td>Nomor Identitas</td>
				<td>:</td>
				<td><input type="text" class="form-control" name="nomor_identitas_member"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td>:</td>
				<td><input type="text" class="form-control" name="email_member"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td><input type="text" class="form-control" name="alamat_member"></td>
			</tr>
			<tr>
				<td>Foto</td>
				<td>:</td>
				<td><input type="file" class="form-contol" name="foto_member"></td>
			</tr>
			<tr>
				<td colspan="3"><button type="submit">Simpan Data</button></td>
			</tr>
		</table> -->
	</div>
</div>
<script>
	var input_nama_lengkap = document.getElementById("nama_lengkap");
	var input_kode_pengguna = document.getElementById("kode_pengguna");
	var input_no_identitas = document.getElementById("no_identitas");
	var input_email = document.getElementById("email");
	var input_alamat = document.getElementById("alamat");
	var btnsimpan = document.getElementById("btnsimpan");
	var btnchange = document.getElementById("btnchange");
	var input_username = document.getElementById("username");
	var input_password = document.getElementById("password");
	var changeusername = document.getElementById("changeusername");
	var status = 1;
	var status2 = 1;
	$(document).ready(function(){
		$('#btnedit').click(function(){
			if(status == 1)
			{
				input_nama_lengkap.readOnly=false;
				input_kode_pengguna.readOnly=false;
				input_no_identitas.readOnly=false;
				input_email.readOnly=false;
				input_alamat.readOnly=false;
				btnsimpan.disabled=false;
				btnchange.disabled=false;
				$('#btnedit').text("Batal");
				status = 0;
			} else
			{
				input_nama_lengkap.readOnly=true;
				input_kode_pengguna.readOnly=true;
				input_no_identitas.readOnly=true;
				input_email.readOnly=true;
				input_alamat.readOnly=true;
				btnsimpan.disabled=true;
				btnchange.disabled=true;
				$('#btnedit').text("Edit Data");
				status = 1;
			}
		});
		$('#btnchange').click(function(){
			changeusername.value = status2;
			if(status2 == 1)
			{
				input_username.readOnly=false;
				input_password.readOnly=false;
				status2 = 0;

			} else
			{
				input_username.readOnly=true;
				input_password.readOnly=true;
				status2 = 1;
			}
		})
	});
</script>