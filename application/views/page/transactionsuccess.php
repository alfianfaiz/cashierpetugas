<div class="fluid-container">
	<center><h2>Selamat Transaksi Berhasil</h2></center>
	<hr>
	<!-- <div class="row"> -->
		<div class="col-md-7 normalpadding normalbox">
			<h4>Rincian Pembelian</h4>
			<table class="table">
				<?php foreach ($this->cart->contents() as $key): ?>
					<tr>
						<td><?php echo $key['name'] ?></td>
						<td><?php echo $this->cart->format_number($key['price']) ?></td>
					</tr>
				<?php endforeach ?>
				<tr>
					<td><b>Total</b></td>
					<td><b><?php echo  $this->cart->format_number($this->cart->total()); ?></b></td>
				</tr>
				<tr>
					<td>Cash</td>
					<td><?php echo $this->session->userdata('cash_bayar'); ?></td>
				</tr>
				<tr>
					<td><b>Kembali</b></td>
					<td><b><?php echo $this->session->userdata('kembali_bayar'); ?></b></td>
				</tr>
			</table>
			<dd>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				
			</dd>
		</div>
		<div class="col-md-4 col-sm-offset-1">
			<div class="btn-group-vertical">
				<button class="btn btn-default form-control">Print Bukti Bayar</button>
				<button class="btn btn-default form-control">Sertifikat Barang</button>
			</div>
		</div>
	<!-- </div> -->
</div>