<?php  
	$harusdibayar = $this->cart->total();

 ?>
<div class="fluid-container">
	<h3>Checkout</h3>
	<hr>
	<form class="form-horizontal col-md-9" method="post" action="<?php echo base_url() ?>administration/dotransaction/">
		<div class="form-group">
		    <label class="col-sm-3">Nominal Harus dibayar</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" name="nominal_bayar" id="harusdibayar" value="<?php echo $harusdibayar ?>" readonly required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3">Cash</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" name="cash_bayar" data-in="" id="cash" placeholder="Rp"  required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3">Kembali</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" name="kembali_bayar" id="kembalian" placeholder="Rp" value="0" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3">Kode Pembeli</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" name="kode_pembeli" placeholder="x-x-x-x" required>
		    </div>
		</div>
		<div class="form-group">
		    <div class="col-sm-12">
		      <button type="submit" class="btn btn-info form-control">Lakukan Transaksi !</button>
		    </div>
		</div>
	</form>
</div>
<script type="text/javascript">
	var harusdibayar = document.getElementById('harusdibayar').value;
	var cash = document.getElementById('cash');

	var cashValue = cash.getAttribute("data-in");
	
	setInterval(function(){
	    if( cash == document.activeElement ){
	    	var temp = cash.value;
		    if( cashValue != temp ){
		       cashValue = temp;
		       cash.setAttribute("data-in",temp);
		       calculate();
		    }
	    }	    
	},50);

	function calculate(){
		document.getElementById('kembalian').value = cashValue-harusdibayar;
	}

	// CARAKEDUA
	// $(document).ready(function(){
	// 	$('#cash').change(function(){

	// 		var cash = document.getElementById('cash').value;
	// 		var harusdibayar = document.getElementById('harusdibayar').value;
	// 		var kembali = cash-harusdibayar;
	// 		// alert('yes');
	// 		// print(kembali);
	// 		document.getElementById('kembalian').value = kembali;
	// 	})
	// })
</script>