<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">	
	<style type="text/css">
		h1
		{
			/*font-weight: lighter;*/
		}
		#container
		{
			font-family:'Lato';
			width:300px;
			/*height:230px;*/
			border:1px solid #ccc;
			padding-bottom:60px;
			margin-top:100px;

		}
		input
		{
			width:76%;
			line-height: 19px;
			margin:5px;
			padding:3px;
			text-align: center
		}
		button
		{
			line-height: 19px;
			background-color:rgba(255,255,255,0.8);
			border:1px solid #ccc;
			min-width:80px;
			padding:3px;
			margin-top:10px;
		}
		button:hover{
			background-color:rgba(85,42,255,0.8);
			color:#fff;
			transition: 0.5s;
			border:1px solid purple;
		}
		.alert{
			font-size: 0.7em;
			color: red;
			font-family: roboto;
		}
	</style>
</head>
<body>

	<center>
		<div id="container">
			<center><h1 class="teks1">Login<b> Cashier</b></h1></center>
			<span class="alert alert-danger">
				<?php echo $this->session->flashdata('error'); ?>
			</span>
			<form class="form-horizontal" action="<?php echo base_url() ?>auth/loginme/" method="post">
			  <div class="form-group">
			    <div class="col-sm-12">
			      <input type="text" class="form-control" name="kode" id="inputEmail3" placeholder="Kode Petugas" maxlength="9">
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-12">
			      <input type="password" class="form-control" name="password" id="inputPassword3" placeholder="Password">
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-12">
			      <center><button type="submit" class="btn btn-success btn-lg">Masuk</button></center>
			    </div>
			  </div>
			</form>
		</div>
	</center>

</body>
</html>