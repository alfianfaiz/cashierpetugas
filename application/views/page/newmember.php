<div class="fluid-container">
	<h2>Daftar Member Baru</h2>	
	<hr>
	<form class="form-horizontal col-md-9" method="post" action="<?php echo base_url() ?>administration/savemember/"   enctype="multipart/form-data">
		<div class="form-group">
		    <label class="col-sm-3">Nama Member</label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" name="nama_register" placeholder="Nama" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3">Kode Member</label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" name="kode_register" placeholder="Kode RFID" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3">Nomor Identitas</label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" name="nomor_identitas_register" placeholder="Nomor Identitas" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3">Email</label>
		    <div class="col-sm-8">
		      <input type="email" class="form-control" name="email_register" placeholder="Email" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3">Username</label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" name="username_register" placeholder="Username" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3">Password</label>
		    <div class="col-sm-8">
		      <input type="password" class="form-control" name="password_register" placeholder="Password" required>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3">Alamat</label>
		    <div class="col-sm-8">
		      <textarea name="alamat_register" class="form-control"></textarea>
		    </div>
		</div>
		<div class="form-group">
		    <label class="col-sm-3">Foto</label>
		    <div class="col-sm-8">
		      <input type="file" name="userfile">
		    </div>
		</div>
		<div class="form-group">
		    <!-- <label class="col-sm-3">Foto</label> -->
		    <div class="col-sm-11">
		      <button type="submit" class="btn btn-primary pull-right">Register</button>
		    </div>
		</div>
	</form>
</div>