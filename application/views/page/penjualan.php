<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Bulan', 'Sales'],
          ['Januari',  1],
          ['Februari',  4],
          ['Maret',  3],
          ['April',  2],
          ['April',  2],
          ['April',  2],
          ['April',  2],
          ['April',  2],
        ]);

        var options = {
          title: 'Riwayat Penjualan',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>

<div class="container-fluid">
	<h4>Rekap Penjualan</h4>
	<hr>
	<form class="form-inline">
		<div class="form-group">
			<!-- <div class="col-md-3"> -->
				<select class="form-control">
					<option>Tahun Ini</option>
					<option>Januari</option>
					<option>Februari</option>
				</select>
				<dd>Ganti Pilihan untuk melihat data berdasarkan bulan tertentu</dd>
			<!-- </div> -->
		</div>
		<div id="curve_chart" style="width: 100%; height: 400px"></div>
		<!-- <div class="form-group">
			<input type="text" name="rfid" placeholder="RFID Kode Petugas" class="form-control">
		</div>
		<div class="form-group">
			<button class="btn btn-default">Lihat Data !</button>
		</div> -->
	</form>
</div>