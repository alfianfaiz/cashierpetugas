<div class="fluid-container">
	<center><h2>Selamat Registrasi Berhasil</h2></center>
	<hr>
	<div class="normalbox" style="padding:10px;">
		<h4>Detail</h4>
		<table class="table">
			<tr>
				<td>Kode Member</td>
				<td>:</td>
				<td><?php echo $this->session->userdata('datauserregistrasi')['kode_pengguna']; ?></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td>:</td>
				<td><?php echo $this->session->userdata('datauserregistrasi')['nama_lengkap']; ?></td>
			</tr>
			<tr>
				<td>Nomor Identitas</td>
				<td>:</td>
				<td><?php echo $this->session->userdata('datauserregistrasi')['no_identitas']; ?></td>
			</tr>
			<tr>
				<td>Email</td>
				<td>:</td>
				<td><?php echo $this->session->userdata('datauserregistrasi')['email']; ?></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td><?php echo $this->session->userdata('datauserregistrasi')['alamat']; ?></td>
			</tr>
			<tr>
				<td>Foto</td>
				<td>:</td>
				<td><img src="<?php echo base_url() ?>uploads/avatar/<?php echo $this->session->userdata('datauserregistrasi')['foto']; ?>" width="200px"></td>
			</tr>
		</table>
	</div>
</div>