<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('cashierpetugas_IS_LOGGED_IN'))
		{
			redirect('auth','refresh');
		}
		$this->load->library('cart');
		
	}

	public function index()
	{
		$data = array(
		"page" => 'home'
		);
		$this->load->view('layout/mainlayout', $data);
	}

	public function penjualan()
	{
		$data = array(
			"page"=> "penjualan"
			);
		$this->load->view('layout/mainlayout', $data);
	}
	public function membership()
	{
		$data = array(
			"page"=> "membership"
			);
		$this->load->view('layout/mainlayout', $data);
	}
	public function transaksi()
	{
		$data = array(
			"page"=> "transaksi"
			);
		$this->load->view('layout/mainlayout', $data);
	}

	public function checkout()
	{
		$data = array(
			"page"=> "checkout"
			);
		$this->load->view('layout/mainlayout', $data);
	}

	public function dotransaction(){
		$this->load->model('User');
		// PEMBELIAN
		$ID_PETUGAS = $this->session->userdata('cashierpetugas_id_user');
		$ID_MEMBER = $this->User->getUserIDByRFID($this->input->post('kode_pembeli'));


		if($ID_MEMBER == 0)
		{
			$datapembelian = array(
				'total_bayar' => $this->input->post('nominal_bayar'),
				'id_member' => $ID_MEMBER,
				'id_petugas' => $ID_PETUGAS,
				);
			// $this->db->insert('pembelian', $datapembelian);
			$INSERT_ID_PEMBELIAN = $this->db->insert_id();
			foreach ($this->cart->contents() as $key) {
				// $this->db->where('id_unit_barang', $key['id']);
				// $this->db->set('pembelian_id_pembelian', $INSERT_ID_PEMBELIAN);
				// $this->db->update('unit_barang');
			}
			$databayar = array(
				'cash_bayar' => $this->input->post('cash_bayar'),
				'kembali_bayar' => $this->input->post('kembali_bayar'),
				);
			print_r($this->session->userdata('cash_bayar'));
			$this->session->set_userdata($databayar);

			// redirect('administration/transactionsuccess','refresh');
		}
		
	}

	public function transactionsuccess()
	{
		$data = array(
			"page"=> "transactionsuccess"
			);
		$this->load->view('layout/mainlayout', $data);
		$this->cart->destroy();
		$databayar = array(
			'cash_bayar' => 0,
			'kembali_bayar' => 0,
		);
		$this->session->set_userdata($databayar);
	}

	public function addBarang(){
		$nomor_unit = $this->input->post('rfid_barang');
		$this->load->model('Barang');
		$databarang = $this->Barang->getDataBarangByNomorUnit($nomor_unit);
		$cek = 0;
		foreach ($databarang as $key) {
			$cek++;
			$data = array(
				'id' => $key->id_unit_barang,
				'price' => $key->harga,
				'qty' => 1,
				'name' => $key->nama_barang,
				'options' => array(
					'Jenis' => $key->nama_jenis,
					'Bahan' => $key->nama_bahan,
					'Warna' => $key->nama_warna,
					'Foto' => $key->foto_barang,
					'nomor_unit' => $key->nomor_unit,
					),
		 	);
		}
		if($cek == 0){
			$this->session->set_flashdata('error', 'Barang tidak ditemukan atau sudah dibeli');
			redirect('administration/transaksi/');	
		}
		$this->cart->insert($data);
		redirect('administration/transaksi/');
		// foreach ($this->cart->contents() as $items) {
		// 	echo $items['options']['Jenis'];
		// }
	}

	public function removebarang($row_id = null)
	{
		$data = array(
			'rowid' => $row_id,
			'qty' => 0,
			 );
		
		$this->cart->update($data);
		redirect('administration/transaksi/');
	}

	public function newmember()
	{
		$data = array(
			"page"=> "newmember"
			);
		$this->load->view('layout/mainlayout', $data);
	}

	public function savemember(){

		$this->form_validation->set_rules('nama_register', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('nomor_identitas_register', 'Nomor Identitas', 'required');
		$this->form_validation->set_rules('kode_register', 'Kode RFID', 'required');
		$this->form_validation->set_rules('email_register', 'Email', 'required');
		$this->form_validation->set_rules('alamat_register', 'Alamat', 'required');
		$this->form_validation->set_rules('password_register', 'Password', 'required');
		$this->form_validation->set_rules('username_register', 'Username', 'required');
		
		if ($this->form_validation->run() == TRUE) {
			$data = array(
					'nama_lengkap' => $this->input->post('nama_register'),
					'no_identitas' => $this->input->post('nomor_identitas_register'),
					'kode_pengguna' => $this->input->post('kode_register'),
					'alamat' => $this->input->post('alamat_register'),
					'email' => $this->input->post('email_register'),
					'password' => $this->input->post('password_register'),
					'level' => '3',
					'status' => 1,
					'username' => $this->input->post('username_register'),
					);
			
			// print_r($this->session->flashdata('datauserregistrasi'));
			$this->db->insert('user', $data);
			$id_user = $this->db->insert_id();

			$fileName =  $id_user.date('Ymdhis').'.jpg';
			$config['upload_path'] 			= './uploads/avatar';
			$config['allowed_types']        = 'png|jpg|jpeg|bmp';
	        $config['max_size']             = 2000;
	        $config['file_name']            = $fileName;
	        $config['overwrite']			= TRUE;

	        // initializing library upload after config
	        $this->load->library('upload', $config);

	        // uploading file
	        if ( ! $this->upload->do_upload('userfile'))
	        {
	        	echo $this->upload->display_errors();
	            // $error = array('error' => $this->upload->display_errors());
	            // $message = "";
	            // foreach ($error as $key) {
	            // 	echo $message .= "<li>".$key."</li>";
	            // }
	            // $this->session->set_flashdata('error',$message);
	        } else{
	        	echo "success";
	        	$this->db->where('id_user', $id_user);
	        	$this->db->set('foto',$fileName);
	        	$this->db->update('user');
	        	$data['foto'] = $fileName;
	        	$this->session->set_userdata('datauserregistrasi',$data);
	        	redirect('administration/registrationsuccess','refresh');
	        }
	        // redirect('administration/registrationsuccess','refresh');
		} else {
			echo validation_errors();
		}		
	}

	public function registrationsuccess()
	{
		$data = array(
			"page"=> "registrationsuccess"
			);
		$this->load->view('layout/mainlayout', $data);
	}

	public function datamember()
	{
		$datamember = array();
		$this->load->model('Pembelian');
		if($this->input->get('rfid'))
		{
			$this->load->model('User');
			$datamember = $this->User->getDataUserByRFID($this->input->get('rfid'));
		}
		$data = array(
			"page"=> "datamember",
			"datamember" => $datamember,
			);
		$this->load->view('layout/mainlayout', $data);
	}

	public function cobaflashdata()
	{
		$arrayName = array('jal' => 'bisaora', );
		$this->session->set_flashdata('dataam', $arrayName);
		echo $this->session->flashdata('dataam')['jal'];
	}

	public function updatemember(){
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('no_identitas', 'Nomor Identitas', 'required');
		$this->form_validation->set_rules('kode_pengguna', 'Kode RFID', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		
		if ($this->form_validation->run() == TRUE) {
			$data = array(
				'nama_lengkap' => $this->input->post('nama_lengkap'),
				'no_identitas' => $this->input->post('no_identitas'),
				'kode_pengguna' => $this->input->post('kode_pengguna'),
				'alamat' => $this->input->post('alamat'),
				'email' => $this->input->post('email'),
				'level' => '3',
				'status' => 1,
				);
			$this->db->where('id_user', $this->input->post('id_member'));
			$this->db->update('user', $data);

			if ($this->input->post('changeusername')) 
			{
				$data = array(
					'username' => $this->input->post('username'),
					);
				$this->db->where('id_user', $this->input->post('id_member'));
				$this->db->update('user', $data);
			
				if($this->input->post('password'))
				{
					$data = array(
					'username' => $this->input->post('username'),
					);
					$this->db->where('id_user', $this->input->post('id_member'));
					$this->db->update('user', $data);
				}
			}
			$this->session->set_flashdata('success', 'Update Data Berhasil !');
			redirect('administration/datamember?rfid='.$this->input->post('kode_pengguna'));

		} else {
			$this->session->set_flashdata('error', 'Update Data Gagal !');
			redirect('administration/datamember?rfid='.$this->input->post('kode_pengguna'));
		}
	}
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */