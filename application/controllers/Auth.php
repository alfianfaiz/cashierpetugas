<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('cashierpetugas_IS_LOGGED_IN')) {
        			redirect('administration','refresh');
        }
		$data = array(
			"page" => 'login'
			);
		$this->load->view('page/login');
	}

	public function loginme()
	{
		if($_POST){			
			$this->form_validation->set_rules('kode', 'Kode Pengguna', 'required');
        	$this->form_validation->set_rules('password', 'Password', 'required');

        	if ($this->form_validation->run() == TRUE) {
        		$this->db->where('kode_pengguna',$this->input->post('kode'));
        		$this->db->where('password',$this->input->post('password'));
        		$this->db->where('level',2);
        		$query = $this->db->get('user');
        		foreach ($query->result() as $q) {
        			$dataDB=array(
						'cashierpetugas_IS_LOGGED_IN' =>TRUE , 
						'cashierpetugas_id_user'=> $q->id_user, 
						'cashierpetugas_level' => $q->level,
						// 'nama'=> $q->nama, 
					);
					$this->session->set_userdata($dataDB);
        		}
        		if ($this->session->userdata('cashierpetugas_IS_LOGGED_IN')) {
        			// echo "you are successfully logged in";
        			 redirect('administration','refresh');
        			return 0;
        		}
        		$this->session->set_flashdata('error', 'Nama atau Kode Pendaftaran yang anda masukkan salah');
				$this->index();

        	} else {
        		$this->session->set_flashdata('error', 'Nama atau Kode Pendaftaran yang anda masukkan salah');
        		$this->index();
        	}
		} 
		else
			redirect('auth/');
	}

	public function out()
	{
		$this->session->sess_destroy();
        redirect(base_url());
	}

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */